#! /usr/bin/env python3

import argparse
import base64
import binascii
import math
import sys

def chunks(data, chunksize):
    return [data[x:x+chunksize] for x in range(0, len(data), chunksize)]

parser = argparse.ArgumentParser()

dataformat = parser.add_mutually_exclusive_group(required=True)
dataformat.add_argument("--hex", "-x", action="store_true")
dataformat.add_argument("--base64", "-b", action="store_true")
parser.add_argument("data")
args = parser.parse_args()

try:
    if args.base64:
        data = base64.b64decode(args.data)
    elif args.hex:
        if args.data.startswith("0x"):
            data = binascii.unhexlify(args.data[2:])
        else:
            data = binascii.unhexlify(args.data)
    data = binascii.hexlify(data).decode("utf-8")
    num_colors = math.floor(len(data)/6)
    colors = chunks(data[:(num_colors*6)], 6)
    extra_bytes = chunks(data[(num_colors*6):], 2)
except:
    print("Invalid data", file=sys.stderr)
    sys.exit(1)

flag_height = math.floor((len(colors)*100)*0.6)

print("""<?xml version="1.0" standalone="no"?>
<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" 
  "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
<svg width="%i" height="%i"
     xmlns="http://www.w3.org/2000/svg" version="1.1">""" % (
    len(colors)*100, flag_height))

position_counter = 0
for color in colors:
    print("""  <rect x="%i" y="0" width="100" height="%i"
        fill="#%s" stroke-width="0"/>""" % (position_counter,
        flag_height, color))
    position_counter += 100

if extra_bytes == []:
    pass
elif len(extra_bytes) == 1:
    print("""  <text style="font-size: 60px;font-family:monospace;fill: #ffffff;stroke:#000000;stroke-width:2px"
        x="%i" y="%i">+</text>""" % ((len(colors)*100)-145, flag_height-20))
    print("""  <text style="font-size: 60px;font-family:monospace;fill: #ffffff;stroke:#000000;stroke-width:2px"
        x="%i" y="%i">%s</text>""" % ((len(colors)*100)-85, flag_height-20, extra_bytes[0].upper()))
elif len(extra_bytes) == 2:
    print("""  <text style="font-size: 60px;font-family:monospace;fill: #ffffff;stroke:#000000;stroke-width:2px"
        x="%i" y="%i">+</text>""" % ((len(colors)*100)-245, flag_height-20))
    print("""  <text style="font-size: 60px;font-family:monospace;fill: #ffffff;stroke:#000000;stroke-width:2px"
        x="%i" y="%i">%s</text>""" % ((len(colors)*100)-185, flag_height-20, extra_bytes[0].upper()))
    print("""  <text style="font-size: 60px;font-family:monospace;fill: #ffffff;stroke:#000000;stroke-width:2px"
        x="%i" y="%i">%s</text>""" % ((len(colors)*100)-85, flag_height-20, extra_bytes[1].upper()))

print("""</svg>""")
